 info-beamer ansible role
=================

Ansible role to install info-beamer on Raspberry Pi Devices.

For more information about info-beamer please visit [info-beamer.com](https://info-beamer.com/product/info-beamer-pi)

For customizing options of this role please have a look into the default variables!


 deploy info-beamer content
-----------------------------

The Info-Beamer content should be stored in a different git repository.

This will be dfined in ``infobeamer_content_repo`` variable.

The content in ths repo should be in the subfolder ``content``.

